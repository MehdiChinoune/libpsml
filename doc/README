To generate a local copy of the documentation, go to the
top level and invoke the FORD documentation command:

    ford -d src -d examples libpsml.md

You can then open the /tmp/psml-docs/index.html file in your
browser.

(You can find information about FORD in
   https://github.com/cmacmackin/ford )

Alternatively, you can browse the documentation online at
the GitHub pages site:

   https://siesta-project.github.io/psml-docs/


NOTES FOR DEVELOPERS

The GitHub Pages site can be set up and updated semi-automatically, with
some subtleties:

- There should be a (separate) working directory associated to the
  GitHub repository 

      git@github.com:siesta-project/psml-docs.git

- The 'ford' command writes its output to /tmp/psml-docs (see
  'libpsml.md' at the top level). This is so since FORD obliterates
  everthing under the target directory when it runs, including any
  .git directory found there.

- It is then necessary to update the contents of the 'psml-docs'
  source working directory. This is done using the 'sync.sh' file
  in that directory, which runs an 'rsync' process. Execute, in
  the working directory:

     sh sync.sh

- Finally, the changes need to be 'git add'ed:

    git add -A

  committed

    git commit -m"Docs update"

  and pushed

    git push

For reference, the contents of the 'sync.sh' file are
reproduced here:
---------------------------------------
#!/bin/sh
#
# Use this file to sync the contents of FORD's target directory
# and the sources for the GitHub Pages documentation
#
# Note the exclusions, to avoid removing the Git control file and this very same file.
#
FORD_TARGET=/tmp/psml-docs/   # Note trailing slash
rsync -av --delete --exclude=README.md \
          --exclude=.git \
          --exclude=sync.sh ${FORD_TARGET} .
---------------------------------------


