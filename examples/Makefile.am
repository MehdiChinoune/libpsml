# Copyright (C) 2017 Yann Pouillon

#
# Makefile for the examples of LibPSML
#

# Connect with the main source directory
AM_FCFLAGS = -I. -I../src

# Source files for the common modules and routines
psml_examples_srcs = \
  custom_psml_die.F90 \
  interpolate_drh.f90 \
  m_getopts.f90

# Fortran modules
# Note: never expect they will have the same name as their source
MODEXT = @ax_cv_f90_modext@
if F90_MOD_UPPERCASE
psml_examples_mods = \
  M_GETOPTS.$(MODEXT)
else
psml_examples_mods = \
  m_getopts.$(MODEXT)
endif

# Libraries
noinst_LTLIBRARIES = libpsml_examples.la

libpsml_examples_la_SOURCES = $(psml_examples_srcs)

# Main example programs
noinst_PROGRAMS = getz normalize show_psml

check_PROGRAMS = getz show_psml normalize

getz_SOURCES = getz.F90
getz_LDADD = libpsml_examples.la ../src/libpsml.la

normalize_SOURCES = normalize.f90
normalize_LDADD = libpsml_examples.la ../src/libpsml.la

show_psml_SOURCES = show_psml.F90
show_psml_LDADD = libpsml_examples.la ../src/libpsml.la

#TESTS = getz show_psml normalize
TESTS = run_getz.sh run_normalize.sh run_show_psml.sh run_show_psml_mgga.sh

# Clean-up non-standard files
CLEANFILES = $(psml_examples_mods)

# Explicit dependencies
getz.$(OBJEXT) normalize.$(OBJEXT) show_psml.$(OBJEXT): libpsml_examples.la

# Make sure everything important is distributed
EXTRA_DIST = \
  bad.psml \
  29_Cu_m.psml \
  mgga.psml \
  14_Si.psml \
  14_Si_UPF_r.psml \
  52_Te_r.psml \
  80_Hg-siesta-vnl.psml \
  80_Hg.psml \
  83_Bi_r.psml \
  Ba.sc-ionic-siesta-vnl.psml \
  Ba.sc-ionic.psml \
  Fe.spin-siesta-vnl.psml \
  Fe.spin.psml \
  README \
  Si.tm2.nrl.vnl.psml \
  custom_psml_die.F90 \
  reference_output/Ba.sc-ionic-siesta-vnl.show \
  reference_output/Proj.scalar_relativistic.0.1 \
  reference_output/Proj.scalar_relativistic.0.2 \
  reference_output/Proj.scalar_relativistic.1.1 \
  reference_output/Proj.scalar_relativistic.1.2 \
  reference_output/Proj.scalar_relativistic.2.1 \
  reference_output/Proj.scalar_relativistic.3.1 \
  reference_output/Pswf.scalar_relativistic.4f \
  reference_output/Pswf.scalar_relativistic.5d \
  reference_output/Pswf.scalar_relativistic.5p \
  reference_output/Pswf.scalar_relativistic.5s \
  reference_output/Valence.charge \
  reference_output/Vlocal \
  reference_output/Vsl.scalar_relativistic.4f \
  reference_output/Vsl.scalar_relativistic.5d \
  reference_output/Vsl.scalar_relativistic.5p \
  reference_output/Vsl.scalar_relativistic.5s \
  reference_output/Vsl.spin_orbit.4f \
  reference_output/Vsl.spin_orbit.5d \
  reference_output/Vsl.spin_orbit.5p \
  run_test.sh \
  run_getz.sh \
  run_normalize.sh \
  run_show_psml.sh \
  run_show_psml_mgga.sh \
  getz_stat.F90


